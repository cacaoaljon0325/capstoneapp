import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  imgURL;

  constructor(private alertController: AlertController, private camera:Camera) { }

  ngOnInit() {
  }

  async click(){
    const alert = await this.alertController.create({
      message:'ADD PHOTO',
      backdropDismiss: true,
      buttons:[{
        text:'Camera',
        handler: data => {
          this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL
          }).then((res)=>{
            this.imgURL = 'data:image/jpeg;base64,' + res;
          }).catch(e =>{
            console.log(e);
          })
        }
        },
      {
        text:'Gallery',
        role:'Gallery',
        handler: (value:any) => {
          console.log(' ')}
      }]
    });

    await alert.present();
  }

}
